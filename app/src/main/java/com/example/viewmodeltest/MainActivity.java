package com.example.viewmodeltest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.SavedStateViewModelFactory;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.viewmodeltest.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private Button button;
    private ActivityMainBinding binding;
    private MyViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);

        viewModel = new ViewModelProvider(this,new SavedStateViewModelFactory(getApplication(),this)).get(MyViewModel.class);
//        viewModel = new ViewModelProvider(this).get(MyViewModel.class);
//        viewModel.getCurrentNum().observe(this, new Observer<Integer>() {
//            @Override
//            public void onChanged(Integer integer) {
//                textView.setText(String.valueOf(integer));
//            }
//        });

        binding.setData(viewModel);
        binding.setLifecycleOwner(this);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.add(1);
            }
        });
    }
}