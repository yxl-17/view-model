package com.example.viewmodeltest;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.SavedStateHandle;
import androidx.lifecycle.ViewModel;

import com.example.viewmodeltest.databinding.ActivityMainBinding;

public class MyViewModel extends ViewModel {

//    public MutableLiveData<Integer> currentNum;
    private SavedStateHandle handle;
    static final String KEY = "KEY";

    public MyViewModel(SavedStateHandle handle) {
        this.handle = handle;
    }

    public MutableLiveData<Integer> getCurrentNum() {
//        if(currentNum == null) {
//            currentNum = new MutableLiveData<Integer>();
//            currentNum.setValue(0); // 必须在创建实例或设置初值，否则在调用getValue时可能发生空指针异常
//        }
//        return currentNum;

//         这些可以防止进程被杀死是数据丢失
        if(!handle.contains(KEY)){
            handle.set(KEY,0);
        }
        return handle.getLiveData(KEY);
    }

    public void add(int num) {
//        currentNum.setValue(currentNum.getValue() + num);
        getCurrentNum().setValue(getCurrentNum().getValue() + num);
    }

}
